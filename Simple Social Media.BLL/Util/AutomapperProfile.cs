﻿using AutoMapper;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.DAL.Entities;

namespace Simple_Social_Media.BLL.Util
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<ApplicationUser, UserDTO>().ReverseMap();

            CreateMap<UserProfile, UserDTO>()
                .ForMember(u => u.Birthday, x => x.MapFrom(p => p.Birthday))
                .ForMember(u => u.Status, x => x.MapFrom(p => p.Status))
                .ForMember(u => u.PhoneNumber, x => x.MapFrom(p => p.User.PhoneNumber))
                .ForMember(u => u.Id, x => x.MapFrom(p => p.User.Id))
                .ForMember(u => u.UserName, x => x.MapFrom(p => p.FullName))
                .ForMember(u => u.IsBanned, x => x.MapFrom(p => p.User.IsBanned))
                .ForMember(u => u.Email, x => x.MapFrom(p => p.User.Email))
                .ReverseMap();

            CreateMap<Chat, ChatDTO>()
                .ForMember(c => c.UserId, x => x.MapFrom(c => c.Contact.User.Id))
                .ForMember(c => c.ChatId, x => x.MapFrom(c => c.ChatId))
                .ForMember(c => c.IsDeleted, x => x.MapFrom(c => c.IsDeleted))
                .ForMember(c => c.FriendId, x => x.MapFrom(c => c.Contact.Friend.Id))
                .ReverseMap();

            CreateMap<Contact, ContactDTO>()
                .ForMember(c => c.Friend, x => x.MapFrom(c => c.Friend.Id))
                .ForMember(c => c.FriendUserName, x => x.MapFrom(c => c.Friend.UserName))
                .ForMember(c => c.IsDeleted, x => x.MapFrom(c => c.IsDeleted))
                .ForMember(c => c.LastOnline, x => x.MapFrom(c => c.Friend.UserProfile.LastOnline))
                .ReverseMap();


            CreateMap<Message, MessageDTO>()
                .ForMember(m => m.ChatId, x => x.MapFrom(m => m.Chat.ChatId))
                .ForMember(m => m.MessageId, x => x.MapFrom(m => m.MessageId))
                .ForMember(m => m.SenderId, x => x.MapFrom(m => m.SenderId))
                .ForMember(m => m.Text, x => x.MapFrom(m => m.Text))
                .ForMember(m => m.Time, x => x.MapFrom(m => m.Time))
                .ReverseMap();
        }
    }
}
