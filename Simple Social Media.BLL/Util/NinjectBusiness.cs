﻿using AutoMapper;
using Ninject.Modules;
using Simple_Social_Media.DAL.Interfaces;
using Simple_Social_Media.DAL.Repositories;

namespace Simple_Social_Media.BLL.Util
{
    public class NinjectBusiness : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();

            Bind<IMapper>().ToMethod(context =>
            {
                var config = new MapperConfiguration(cfg =>
                  {
                      cfg.AddProfile<AutomapperProfile>();
                  });
                return config.CreateMapper();
            });
        }
    }
}
