﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface ICrud<T,Type> 
        where T : class
    {
        Task<T> GetByIdAsync(Type id);
        Task CreateAsync(T model);
        Task UpdateAsync(T model);
        Task DeleteByIdAsync(Type id);
    }
}
