﻿using Simple_Social_Media.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface IContactService : ICrud<ContactDTO,int>, IDisposable
    {
        Task<IEnumerable<ContactDTO>> GetFriendsAsync(string userId);
    }
}
