﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface IUserService : ICrud<UserDTO,string>, IDisposable
    {
        #region Network
        bool IsUserAdmin(string id);
        Task<bool> IsUserAdminAsync(string id);
        Task<bool> IsUserExistsAsync(string id);        
        Task<bool> IsUserBannedAsync(string id);
        Task<bool> IsUserBannedAsync(UserDTO id);
        Task<OperationDetails> CreateAccountAsync(UserDTO model);
        Task<ClaimsIdentity> Authenticate(UserDTO user);
        Task<IEnumerable<UserDTO>> GetAll();
        Task<IEnumerable<UserDTO>> GetAllWithoutCurrent(string curentUserId);
        Task<IEnumerable<UserDTO>> FindUsers(UserDTO filters);
        #endregion

        #region Additional functionality
        Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool remember, bool shouldLockout = false);
        Task<bool> HasBeenVerifiedAsync();
        Task<SignInStatus> TwoFactorSignInAsync(string provider, string code, bool isPersistent, bool rememberBrowser);
        Task<IdentityResult> ConfirmEmailAsync(string userId, string code);
        Task<UserDTO> FindByEmailAsync(string email);
        Task<bool> IsEmailConfirmedAsync(string userId);
        Task<UserDTO> FindByNameAsync(string userName);
        Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword);
        Task<string> GetVerifiedUserIdAsync();
        Task<IList<string>> GetValidTwoFactorProvidersAsync(string userId);
        Task<bool> SendTwoFactorCodeAsync(string provider);
        Task<SignInStatus> ExternalSignInAsync(ExternalLoginInfo loginInfo, bool isPersistent);
        Task<IdentityResult> CreateWithIdentityAsync(UserDTO userDTO);
        Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);
        Task SignInAsync(UserDTO userDTO, bool isPersistent, bool rememberBrowser);
        Task<string> GetPhoneNumberAsync(string userId);
        Task<bool> GetTwoFactorEnabledAsync(string userId);
        Task<IList<UserLoginInfo>> GetLoginsAsync(string userId);
        Task<IdentityResult> RemoveLoginAsync(string userId, UserLoginInfo login);
        Task<UserDTO> FindByIdAsync(string userId);
        Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string phoneNumber);
        Task<IdentityResult> SetTwoFactorEnabledAsync(string userId, bool enabled);
        Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string token);
        Task<IdentityResult> SetPhoneNumberAsync(string userId, string phoneNumber);
        Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task<IdentityResult> AddPasswordAsync(string userId, string password);
        UserDTO FindById(string userId);
        #endregion
    }
}
