﻿using Simple_Social_Media.BLL.DTO;
using System;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface IMessageService : ICrud<MessageDTO,int>, IDisposable
    {
    }
}
