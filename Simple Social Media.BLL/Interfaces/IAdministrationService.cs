﻿using System;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface IAdministrationService : IDisposable
    {
        Task BanUser(string userId);
        Task UnBanUser(string userId);
    }
}
