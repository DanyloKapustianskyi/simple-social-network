﻿using Simple_Social_Media.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Interfaces
{
    public interface IChatService : ICrud<ChatDTO,int>, IDisposable
    {
        Task SendMessage(MessageDTO message);
        Task RestoreChat(int id);
        Task<ChatDTO> GetChat(ChatDTO chat);
        Task<IEnumerable<MessageDTO>> GetMessagesAsync(int id);
        Task<IEnumerable<ChatDTO>> GetAllUserChats(string userId);
    }
}
