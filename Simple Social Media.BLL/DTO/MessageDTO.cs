﻿using System;

namespace Simple_Social_Media.BLL.DTO
{
    public class MessageDTO
    {
        public int MessageId { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public int ChatId { get; set; }
        public string SenderId { get; set; }
    }
}
