﻿using System;

namespace Simple_Social_Media.BLL.DTO
{
    public class UserDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBanned { get; set; }
        public DateTime Birthday { get; set; }
        public string Status { get; set; }
        public string Role { get; set; }
        public DateTime LastOnline { get; set; }
    }
}
