﻿using System;

namespace Simple_Social_Media.BLL.DTO
{
    public class ChatDTO
    {
        public int ChatId { get; set; }
        public string UserId { get; set; }
        public string FriendName { get; set; }
        public string FriendId { get; set; }
        public bool IsDeleted { get; set; }
        public string LastMessage { get; set; }
        public DateTime MessageDateTime { get; set; }
    }
}
