﻿using System;

namespace Simple_Social_Media.BLL.DTO
{
    public class ContactDTO
    {
        public int ContactId { get; set; }
        public string User { get; set; }
        public string Friend { get; set; }
        public string FriendUserName { get; set; }
        public DateTime LastOnline { get; set; }
        public bool IsDeleted { get; set; }
    }
}
