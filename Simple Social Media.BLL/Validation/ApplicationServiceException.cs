﻿using System;

namespace Simple_Social_Media.BLL.Validation
{
    [Serializable]
    public class ApplicationServiceException : Exception
    {
        public string Property { get; protected set; }
        public ApplicationServiceException() { }
        public ApplicationServiceException(string message) : base(message) { }
        public ApplicationServiceException(string message, string prop) : base(message)
        {
            Property = prop;
        }
    }
}
