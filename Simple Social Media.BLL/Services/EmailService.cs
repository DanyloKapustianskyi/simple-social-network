﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Services
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Подключите здесь службу электронной почты для отправки сообщения электронной почты.
            return Task.FromResult(0);
        }
    }
}
