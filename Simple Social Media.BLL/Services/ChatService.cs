﻿using AutoMapper;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Validation;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Services
{
    public class ChatService : IChatService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public ChatService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        public async Task CreateAsync(ChatDTO model)
        {
            CheckChat(model);
            IEnumerable<Chat> chats = await unitOfWork.ChatRepository.GetAllAsync();
            Chat chat = mapper.Map<Chat>(model);
            unitOfWork.ChatRepository.Create(chat);
            await unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            Chat chat = await Task.Run(async () => {
                IEnumerable<Chat> chats = await unitOfWork.ChatRepository.GetAllAsync();
                return chats.Where(c => c.ChatId == id)
                   .FirstOrDefault();
                  });
            if (chat != null)
            {
                chat.IsDeleted = true;
                await unitOfWork.SaveAsync();
            }
            else
            {
                throw new ApplicationServiceException($"Chat was not found with id - {id}");
            }

        }


        public async Task<IEnumerable<ChatDTO>> GetAllUserChats(string userId)
        {
            IEnumerable<Chat> chats = await unitOfWork.ChatRepository.GetAllAsync();
            chats = chats.Where(c => c.Contact.User.Id == userId || c.Contact.Friend.Id == userId)
                .Where(c=>c.IsDeleted==false);
            IEnumerable<ChatDTO> chatsDTO= mapper.Map<IEnumerable<ChatDTO>>(chats);
            foreach(var chat in chatsDTO)
            {
                Message message =await GetMessage(chat);
                chat.LastMessage = message.Text;
                chat.MessageDateTime = message.Time;
                UserProfile friend = await unitOfWork.UserProfileRepository.GetById(chat.FriendId);
                chat.FriendName = friend.FullName;
            }
            return chatsDTO;
        }

        public async Task<ChatDTO> GetByIdAsync(int id)
        {
            Chat chat = await unitOfWork.ChatRepository.GetById(id);
            if (chat == null)
            {
                throw new ApplicationServiceException($"Chat with id-{id} was not found");
            }
            ChatDTO result = mapper.Map<ChatDTO>(chat);
            return await Task.Run(async () =>
            {
                Message message = await GetMessage(result);

                result.LastMessage = message.Text;
                result.MessageDateTime = message.Time;
                return result;
            });
        }

        public async Task<ChatDTO> GetChat(ChatDTO chat)
        {
            CheckChat(chat);

            Message message = await GetMessage(chat);

            return await Task.Run(() =>
            {
                chat.LastMessage = message.Text;
                chat.MessageDateTime = message.Time;
                chat.FriendName = message.Chat.Contact.Friend.UserName;
                return chat;
            });
        }

        public async Task<IEnumerable<MessageDTO>> GetMessagesAsync(int id)
        {
            return mapper.Map<IEnumerable<MessageDTO>>(await Task.Run(async () => {
                IEnumerable<Message> messages = await unitOfWork.MessageRepository.GetAllAsync();
                return messages.Where(m => m.Chat.ChatId == id)
                .OrderBy(m => m.Time);
                }));
        }

        public async Task RestoreChat(int id)
        {
            Chat chat = await Task.Run(() => unitOfWork.ChatRepository.GetById(id));
            if (chat == null)
            {
                throw new ApplicationServiceException($"Chat with id-{id} was not found");
            }

            chat.IsDeleted = false;
            await unitOfWork.SaveAsync();
        }

        public async Task SendMessage(MessageDTO message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            Message newMessage = mapper.Map<Message>(message);
            unitOfWork.MessageRepository.Create(newMessage);
            await unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(ChatDTO model)
        {
            CheckChat(model);
            Chat chat = mapper.Map<Chat>(model);
            unitOfWork.ChatRepository.Update(chat);
            await unitOfWork.SaveAsync();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        private void CheckChat(ChatDTO model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
        }

        private async Task<Message> GetMessage(ChatDTO chat)
        {
            IEnumerable<Message> messages=await unitOfWork.MessageRepository.GetAllAsync();

            return  messages
                .Where(m => m.Chat.ChatId == chat.ChatId)
                .OrderByDescending(m => m.Time)
                .FirstOrDefault();
        }
    }
}
