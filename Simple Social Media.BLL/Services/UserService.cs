﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Infrastructure;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Validation;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IContactService contactService;

        #region Network
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.contactService = new ContactService(unitOfWork,mapper);
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO user)
        {
            if (user == null)
            {
                throw new ApplicationServiceException($"{nameof(user)} is null");
            }

            ClaimsIdentity claim=null;
            ApplicationUser userAuth = await unitOfWork.UserManager.FindAsync(user.Email, user.Password);
            if (userAuth != null)
            {
                claim = await unitOfWork.UserManager.CreateIdentityAsync(userAuth, DefaultAuthenticationTypes.ApplicationCookie);
            }
            return claim;
        }

        public async Task<OperationDetails> CreateAccountAsync(UserDTO model)
        {
            if (model == null)
            {
                throw new ApplicationServiceException($"{nameof(model)} is null");
            }

            var user = await unitOfWork.UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                return new OperationDetails(false, "Аккаунт с такой почтой уже существует", $"{model.Email}");

            }
            else
            {
                user = new ApplicationUser()
                {
                    Email = model.Email,
                    UserName = model.Email,
                    PhoneNumber=model.PhoneNumber,
                };

                IdentityResult result = await unitOfWork.UserManager.CreateAsync(user, model.Password);
                if (result.Errors.Count() != 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), $"{nameof(result)}");
                }
                else
                {
                    await unitOfWork.UserManager.AddToRoleAsync(user.Id, model.Role);
                    UserProfile userProfile = mapper.Map<UserProfile>(model);
                    userProfile.Id = user.Id;
                    userProfile.User = user;
                    ApplicationUser dbUser = await unitOfWork.UserManager.FindByIdAsync(user.Id);
                    
                    unitOfWork.UserProfileRepository.Create(userProfile);
                    unitOfWork.UserManager.Update(user);
                    await unitOfWork.SaveAsync();

                    return new OperationDetails(true, "Account creation was succesful", "");

                }
            }
        }     

        public async Task<IEnumerable<UserDTO>> FindUsers(UserDTO filters)
        {
            if (filters == null)
            {
                throw new ApplicationServiceException($"{nameof(filters)} is null");
            }

            IEnumerable<UserDTO> users = await GetAll();
            users = users.Where(u => !u.Id.Equals(filters.Id))
                    .Where(u => IsUserAdmin(u.Id) == false);
            

            IEnumerable<ContactDTO> userFriends = await contactService.GetFriendsAsync(filters.Id);

            if (!string.IsNullOrEmpty(filters.UserName))
            {
                users=users.Where(
                    u=>u.UserName
                    .ToLower()
                    .Contains(filters.UserName.ToLower())
                    );
            }
            if (!string.IsNullOrEmpty(filters.PhoneNumber)) 
            {
                 users=users.Where(
                    u=>u.PhoneNumber
                    .Contains(filters.PhoneNumber)
                    );
            }
            if (!string.IsNullOrEmpty(filters.Email))
            {
                 users = users.Where(
                    u => u.Email
                    .ToLower()
                    .Contains(filters.Email.ToLower())
                    );
            }

            return await Task.Run(() => users
                .Where(u=>!userFriends.Any(f=>f.Friend.Equals(u.Id)))
                .Where(u => !u.Id.Equals(filters.Id))
            );
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            return mapper.Map<IEnumerable<UserDTO>>(await Task.Run(()=>unitOfWork.UserProfileRepository.GetAllAsync()));
        }

        public async Task<IEnumerable<UserDTO>> GetAllWithoutCurrent(string curentUserId)
        {
            if (string.IsNullOrEmpty(curentUserId))
            {
                throw new ApplicationServiceException($"{nameof(curentUserId)} is null or empty");
            }
            IEnumerable<UserProfile> users = await Task.Run(() => unitOfWork.UserProfileRepository.GetAllAsync());
            return mapper.Map<IEnumerable<UserDTO>>(await Task.Run(
                () => users.Where(u => !u.User.Id.Equals(curentUserId)))
                );
        }

        public async Task<UserDTO> GetByIdAsync(string id)
        {
            UserProfile profile = await unitOfWork.UserProfileRepository.GetById(id);
            if (profile != null)
            {
                return mapper.Map<UserDTO>(profile);
            }
            else
            {
                throw new ApplicationServiceException($"User profile with {id} was not found");
            }
        }

        public async Task<bool> IsUserAdminAsync(string id)
        {
            CheckIdString(id);

            return await unitOfWork.UserManager.IsInRoleAsync(id, "Admin");
        }

        public bool IsUserAdmin(string id)
        {
            CheckIdString(id);

            return unitOfWork.UserManager.IsInRole(id, "Admin");
        }

        public async Task<bool> IsUserBannedAsync(string id)
        {
            CheckIdString(id);

            ApplicationUser user = await unitOfWork.UserManager.FindByIdAsync(id);
            if (user != null)
            {
                return user.IsBanned;
            }
            else
            {
                throw new ApplicationServiceException($"User with id-{id} was not found");
            }
        }

        public async Task<bool> IsUserBannedAsync(UserDTO user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            ApplicationUser applicationUser = await unitOfWork.UserManager.FindAsync(user.Email, user.Password);
            return applicationUser.IsBanned;
        }

        public async Task<bool> IsUserExistsAsync(string id)
        {
            CheckIdString(id);

            ApplicationUser user = await unitOfWork.UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task UpdateAsync(UserDTO model)
        {
            /*UserProfile profile = await Task.Run(()=>unitOfWork.UserProfileRepository
            .GetAllAsync().Result
            .Where(u=>u.User.Id.Equals(model.Id))
            .FirstOrDefault());*/
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            UserProfile profile= await unitOfWork.UserProfileRepository.GetById(model.Id);

            profile.Birthday = model.Birthday;
            profile.FullName = model.UserName;
            profile.Status = model.Status;
            profile.LastOnline = model.LastOnline;
            profile.User.PhoneNumber = model.PhoneNumber;
            
            unitOfWork.UserProfileRepository.Update(profile);
            await unitOfWork.SaveAsync();
        }

        private void CheckIdString(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                throw new ApplicationServiceException($"{nameof(id)} is null or empty");
            }
        }

        public Task DeleteByIdAsync(string id)
        {
            ApplicationUser user = unitOfWork.UserManager.Users
                .Where(u => u.UserProfile.Id == id)
                .FirstOrDefault();
            user.IsDeleted = true;
            return unitOfWork.SaveAsync();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }

        Task ICrud<UserDTO, string>.CreateAsync(UserDTO model)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Additional functionality
        public async Task<SignInStatus> PasswordSignInAsync(string userName,string password, bool remember, bool shouldLockout = false)
        {
            return await unitOfWork.SignInManager.PasswordSignInAsync(userName, password, remember, shouldLockout);
        }

        public async Task<bool> HasBeenVerifiedAsync()
        {
            return await unitOfWork.SignInManager.HasBeenVerifiedAsync();
        }

        public async Task<SignInStatus> TwoFactorSignInAsync(string provider, string code, bool isPersistent, bool rememberBrowser)
        {
            return await unitOfWork.SignInManager.TwoFactorSignInAsync(provider, code, isPersistent, rememberBrowser);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(string userId,string code)
        {
            return await unitOfWork.UserManager.ConfirmEmailAsync(userId, code);
        }

        public async Task<UserDTO> FindByEmailAsync(string email)
        {

            return mapper.Map<UserDTO>(await unitOfWork.UserManager.FindByEmailAsync(email));
        }

        public async Task<bool> IsEmailConfirmedAsync(string userId)
        {
            return await unitOfWork.UserManager.IsEmailConfirmedAsync(userId);
        }

        public async Task<UserDTO> FindByNameAsync(string userName)
        {
            return mapper.Map<UserDTO>(await unitOfWork.UserManager.FindByNameAsync(userName));
        }

        public async Task<IdentityResult> ResetPasswordAsync(string userId, string token, string newPassword)
        {
            return await unitOfWork.UserManager.ResetPasswordAsync(userId, token, newPassword);
        }

        public async Task<string> GetVerifiedUserIdAsync()
        {
            return await unitOfWork.SignInManager.GetVerifiedUserIdAsync();
        }

        public async Task<IList<string>> GetValidTwoFactorProvidersAsync(string userId)
        {
            return await unitOfWork.UserManager.GetValidTwoFactorProvidersAsync(userId);
        }

        public async Task<bool> SendTwoFactorCodeAsync(string provider)
        {
            return await unitOfWork.SignInManager.SendTwoFactorCodeAsync(provider);
        }

        public async Task<SignInStatus> ExternalSignInAsync(ExternalLoginInfo loginInfo, bool isPersistent)
        {
            return await unitOfWork.SignInManager.ExternalSignInAsync(loginInfo, isPersistent);
        }

        public async Task<IdentityResult> CreateWithIdentityAsync(UserDTO userDTO)
        {
            ApplicationUser user = new ApplicationUser { Email = userDTO.Email, UserName = userDTO.UserName };
            userDTO.Id = user.Id;
            return await unitOfWork.UserManager.CreateAsync(user);
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            return await unitOfWork.UserManager.AddLoginAsync(userId, login);
        }

        public async Task SignInAsync(UserDTO userDTO, bool isPersistent, bool rememberBrowser)
        {
            await unitOfWork.SignInManager.SignInAsync(mapper.Map<ApplicationUser>(userDTO), isPersistent, rememberBrowser);
        }

        public async Task<string> GetPhoneNumberAsync(string userId)
        {
            return await unitOfWork.UserManager.GetPhoneNumberAsync(userId);
        }

        public async Task<bool> GetTwoFactorEnabledAsync(string userId)
        {
            return await unitOfWork.UserManager.GetTwoFactorEnabledAsync(userId);
        }

        public async Task<IList<UserLoginInfo>> GetLoginsAsync(string userId)
        {
            return await unitOfWork.UserManager.GetLoginsAsync(userId);
        }

        public async Task<IdentityResult> RemoveLoginAsync(string userId, UserLoginInfo login)
        {
            return await unitOfWork.UserManager.RemoveLoginAsync(userId, login);
        }

        public async Task<UserDTO> FindByIdAsync(string userId)
        {
            return mapper.Map<UserDTO>(await unitOfWork.UserManager.FindByIdAsync(userId));
        }

        public async Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string phoneNumber)
        {
            return await unitOfWork.UserManager.GenerateChangePhoneNumberTokenAsync(userId, phoneNumber);
        }

        public async Task<IdentityResult> SetTwoFactorEnabledAsync(string userId, bool enabled)
        {
            return await unitOfWork.UserManager.SetTwoFactorEnabledAsync(userId, enabled);
        }

        public async Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string token)
        {
            return await unitOfWork.UserManager.ChangePhoneNumberAsync(userId, phoneNumber, token);
        }

        public async Task<IdentityResult> SetPhoneNumberAsync(string userId, string phoneNumber)
        {
            return await unitOfWork.UserManager.SetPhoneNumberAsync(userId, phoneNumber);
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            return await unitOfWork.UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);
        }

        public async Task<IdentityResult> AddPasswordAsync(string userId, string password)
        {
            return await unitOfWork.UserManager.AddPasswordAsync(userId, password);
        }

        public UserDTO FindById(string userId)
        {
            return mapper.Map<UserDTO>(unitOfWork.UserManager.FindById(userId));
        }

        #endregion
    }
}
