﻿using AutoMapper;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Validation;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Services
{
    public class AdministrationService : IAdministrationService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public AdministrationService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task BanUser(string userId)
        {
            ApplicationUser user = await GetUserToModerate(userId);
            user.IsBanned = true;
            await unitOfWork.SaveAsync();
        }

        public async Task UnBanUser(string userId)
        {
            ApplicationUser user = await GetUserToModerate(userId);
            user.IsBanned = false;
            await unitOfWork.SaveAsync();
        }

        private async Task<ApplicationUser> GetUserToModerate(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ApplicationServiceException($"{nameof(userId)} is null or empty");
            }
            ApplicationUser user = await unitOfWork.UserManager.FindByIdAsync(userId);
            return user;
        }
        public void Dispose()
        {
            unitOfWork.Dispose();
        }
    }
}
