﻿using AutoMapper;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Validation;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Media.BLL.Services
{
    public class ContactService : IContactService
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;

        public ContactService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task CreateAsync(ContactDTO model)
        {
            if (model == null)
            {
                throw new ApplicationServiceException($"{nameof(model)} is null");
            }
            ApplicationUser user = new ApplicationUser();

            var contacts = await unitOfWork.ContactRepository.GetAllAsync();
            var findContact = contacts
                .Where(c => c.User.Id.Equals(model.User))
                .Where(c => c.Friend.Id.Equals(model.Friend))
                .FirstOrDefault();
                if (findContact == null)
                {
                    Contact newContact = new Contact
                    {
                        User = await unitOfWork.UserManager.FindByIdAsync(model.User),
                        Friend = await unitOfWork.UserManager.FindByIdAsync(model.Friend),
                        IsDeleted = false,
                    };
                    unitOfWork.ContactRepository.Create(newContact);
                }
                else if(findContact.IsDeleted)
                {
                    findContact.IsDeleted = false;
                }
            
            await unitOfWork.SaveAsync();
                
        }

        public async Task DeleteByIdAsync(int id)
        {
            var delete = await unitOfWork.ContactRepository.GetById(id);
            delete.IsDeleted = true;
            await unitOfWork.SaveAsync();
        }

        public async Task<ContactDTO> GetByIdAsync(int id)
        {
            Contact contact = await unitOfWork.ContactRepository.GetById(id);
            if (contact == null)
            {
                throw new ApplicationServiceException("Not found contact", nameof(id));
            }
            return mapper.Map<ContactDTO>(contact);

        }

        public async Task<IEnumerable<ContactDTO>> GetFriendsAsync(string userId)
        {
            if (String.IsNullOrEmpty(userId))
            {
                throw new ApplicationServiceException($"{nameof(userId)} is null or empty");
            }

            IEnumerable<Contact> contactsUser = await unitOfWork.ContactRepository.GetAllAsync();
            contactsUser=contactsUser.Where(c => c.User.Id == userId)
                .Where(c => c.IsDeleted == false);
            if (contactsUser == null)
            {
                return null;
            }
            return await Task.Run(()=> mapper.Map<IEnumerable<ContactDTO>>(contactsUser));

        }

        public Task UpdateAsync(ContactDTO model)
        {
            if (model == null)
            {
                throw new ApplicationServiceException($"{nameof(model)} is null");
            }

            Contact findContact = mapper.Map<Contact>(model);
            unitOfWork.ContactRepository.Update(findContact);
            return unitOfWork.SaveAsync();
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }
    }
}
