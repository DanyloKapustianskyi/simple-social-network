﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Simple_Social_Media.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly IAdministrationService administrationService;

        public AdminController(IAdministrationService administrationService)
        {
            this.administrationService = administrationService;
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private string adminId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        public ActionResult Index()
        {
            return RedirectToAction("GetUsers");
        }

        [HttpGet]
        public async Task<ActionResult> GetUsers()
        {
            UserDTO current = await UserService.GetByIdAsync(adminId);
            if (current == null)
            {
                return RedirectToAction("LogOff", "Account");
            }

            AdminViewModel admin = new AdminViewModel
            {
                UserName = current.UserName,
                Users = await UserService.GetAllWithoutCurrent(adminId),
            };
            return View(admin);
        }

        public async  Task<ActionResult> BanUser(string id)
        {
            await administrationService.BanUser(id);
            return RedirectToAction("GetUsers");
        }

        public async Task<ActionResult> UnBanUser(string id)
        {
            await administrationService.UnBanUser(id);
            return RedirectToAction("GetUsers");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserService.Dispose();
                administrationService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}