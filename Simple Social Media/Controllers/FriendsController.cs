﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Simple_Social_Media.Controllers
{
    [Authorize(Roles ="User")]
    public class FriendsController : Controller
    {

        private readonly IContactService contactService;

        private string UserID
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public FriendsController(IContactService contactService)
        {
            this.contactService = contactService;
        }


        [HttpGet]
        public async Task<ActionResult> GetFriends()
        {
            UserDTO user = await UserService.GetByIdAsync(UserID);
            FriendsViewModel friends = new FriendsViewModel();
            friends.UserProfileModel = new UserProfileViewModel
            {
                UserName = user.UserName,
                Status = user.Status,
                Phone = user.PhoneNumber,
                Birthday = user.Birthday,
                Email = user.Email,
                UserId = user.Id,
            };
            try
            {
                friends.Contacts = await contactService.GetFriendsAsync(UserID);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            return View(friends) ;
        }

        [HttpPost]
        public async Task<ActionResult> AddFriend(string id)
        {
            ContactDTO contact = new ContactDTO
            {
                User = UserID,
                Friend = id,
                IsDeleted = false,
            };
            await contactService.CreateAsync(contact);
            return RedirectToAction("GetFriends");
        }

        
        public async Task<ActionResult> FindAccounts(FriendSearchViewModel friendSearch)
        {
            UserDTO filters = new UserDTO
            {
                Id=UserID,
                UserName=friendSearch.Name,
                Email=friendSearch.Email,
                PhoneNumber=friendSearch.Phone,
            };
            friendSearch.SearchedUsers = await UserService.FindUsers(filters);

            return PartialView("PrintFriendsPartial", friendSearch);
        }

       

        [HttpPost]
        public async Task<ActionResult> DeleteFromFriends(int id)
        {
            await contactService.DeleteByIdAsync(id);
            return RedirectToAction("GetFriends");
        }

        
    }
}