﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Simple_Social_Media.Controllers
{
    public class HomeController : Controller
    {
        private string UserID
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        // GET: Friends
        public ActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("GetUsers", "Admin", null);
            }
            return View();
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> EditProfile()
        {
            UserDTO user = await UserService.GetByIdAsync(UserID);
            UserProfileViewModel userProfile = new UserProfileViewModel
            {
                UserName = user.UserName,
                Status = user.Status,
                Phone = user.PhoneNumber,
                Birthday = user.Birthday,
            };
            return View(userProfile);
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> EditProfile(UserProfileViewModel userProfile)
        {
            if (ModelState.IsValid)
            {
                UserDTO user = new UserDTO()
                {
                    Id = UserID,
                    UserName = userProfile.UserName,
                    Status = userProfile.Status,
                    Birthday = userProfile.Birthday,
                    PhoneNumber = userProfile.Phone,
                    LastOnline = DateTime.Now,
                };

                await UserService.UpdateAsync(user);
                userProfile.IsSuccess = true;
            }
            return View(userProfile);
        }
    }
}