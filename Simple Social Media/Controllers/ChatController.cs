﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Simple_Social_Media.BLL.DTO;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Simple_Social_Media.Controllers
{
    [Authorize(Roles ="User")]
    public class ChatController : Controller
    {
        IChatService chatService;

        public ChatController(IChatService service)
        {
            chatService = service;
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private string currentId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            UserDTO currentUser = await UserService.GetByIdAsync(currentId);
            if (currentUser == null)
            {
                return RedirectToAction("SignOut");
            }
            else
            {
                currentUser.Id = currentId;
                UserChatsViewModel userChats = new UserChatsViewModel();
                
                userChats.Chats = await chatService.GetAllUserChats(currentId);
                userChats.UserName = currentUser.UserName;

                userChats.UserProfile = new UserProfileViewModel()
                {
                    UserId=currentId,
                    UserName=currentUser.UserName,
                    Birthday=currentUser.Birthday,
                    Status=currentUser.Status,
                    Email=currentUser.Email,
                    Phone=currentUser.PhoneNumber,
                };


                return View(userChats);
            }
        }

        [HttpGet]
        public async Task<ActionResult> ChatWithMessages(int id)
        {

            ChatDTO chat = await chatService.GetChat(new ChatDTO { ChatId = id });

            ChatViewModel chatView = new ChatViewModel
            {
                ChatId = id,
                UserId = currentId,
                FriendName = chat.FriendName,
                Messages = await chatService.GetMessagesAsync(id),
            };
            chatView.NewMessage = chatView.Messages.LastOrDefault().Text;

            return PartialView(chatView);
        }


        public async Task<ActionResult> SignOut()
        {
            var authenticationManager = HttpContext.GetOwinContext().Authentication;
            await Task.Run(()=>authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie));
            return RedirectToAction("Index", "Home");
        }
    }
}