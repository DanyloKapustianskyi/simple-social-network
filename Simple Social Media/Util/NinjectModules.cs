﻿using Ninject;
using Ninject.Modules;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Util;

namespace Simple_Social_Media.Util
{
    public sealed class NinjectApplicationModules
    {
        private static StandardKernel kernel;

        public static StandardKernel Kernel
        {
            get
            {
                if (kernel == null)
                {
                    NinjectModule ninjectWeb = new NinjectWeb();
                    NinjectModule ninjectBusiness = new NinjectBusiness();
                    kernel = new StandardKernel(ninjectWeb, ninjectBusiness);
                }
                return kernel;
            }
        }

        private NinjectApplicationModules() { }

        public static IUserService CreateUserService()
        {
            return kernel.Get<IUserService>();
        }
    }
}