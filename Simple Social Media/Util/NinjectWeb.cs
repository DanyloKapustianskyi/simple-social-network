﻿using Ninject.Modules;
using Simple_Social_Media.BLL.Interfaces;
using Simple_Social_Media.BLL.Services;

namespace Simple_Social_Media.Util
{
    public class NinjectWeb : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdministrationService>().To<AdministrationService>();
            Bind<IChatService>().To<ChatService>();
            Bind<IContactService>().To<ContactService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}