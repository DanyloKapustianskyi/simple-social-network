﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Simple_Social_Media.Startup))]
namespace Simple_Social_Media
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
