﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется следующим образом
// набора атрибутов. Измените значения этих атрибутов для изменения сведений,
// связанных с этой сборкой.
[assembly: AssemblyTitle("Simple_Social_Media")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Simple_Social_Media")]
[assembly: AssemblyCopyright("© , 2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False в параметре ComVisible делает типы в этой сборке невидимыми
// для компонентов COM.  Если требуется обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение true для требуемого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для ID библиотеки типов typelib, если этот проект видим для COM
[assembly: Guid("1ea8a4fb-b779-434f-8590-c586ef5bc1d4")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      основной номер версии;
//      Дополнительный номер версии
//      номер сборки;
//      редакция.
//
// Можно задать все значения или принять номер редакции и номер сборки по умолчанию,
// используя "*", как показано ниже:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
