﻿using Simple_Social_Media.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Simple_Social_Media.Models
{
    public class AdminViewModel
    {
        public string UserName { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}