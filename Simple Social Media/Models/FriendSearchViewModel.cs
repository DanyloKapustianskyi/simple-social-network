﻿using Simple_Social_Media.BLL.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Simple_Social_Media.Models
{
    public class FriendSearchViewModel
    {
        [Display(Name ="Имя")]
        [RegularExpression(@"\w+",ErrorMessage ="Специальные символы не разрешены")]
        [MaxLength(50,ErrorMessage ="Максимальная длина 50")]
        public string Name { get; set; }

        [Display(Name ="Почта")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage ="Неправильная почта")]
        [MaxLength(50,ErrorMessage ="Максимальная длина 50")]
        public string Email { get; set; }

        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Неправильный номер телефона")]       
        [MaxLength(15, ErrorMessage = "Максимальная длина 15!")]
        public string Phone { get; set; }

        public IEnumerable<UserDTO> SearchedUsers { get; set; }
    }
}