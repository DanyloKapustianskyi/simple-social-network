﻿using Simple_Social_Media.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Simple_Social_Media.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Код")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Запомнить браузер?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Display(Name = "Адрес электронной почты")]
        [Required(ErrorMessage ="Введите вашу почту")]      
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage ="Введите ваш пароль")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        [MaxLength(100,ErrorMessage ="Максимальна длина пароля - 100")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Введите вашу почту")]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Номер")]
        [Phone(ErrorMessage = "Неправильный номер")]
        [Required(ErrorMessage = "Введите ваш номер телефона")]
        [MinLength(10, ErrorMessage = "Минимальная длина 10!")]
        [MaxLength(15, ErrorMessage = "Максимальная длина 15!")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Введите ваше имя")]
        [RegularExpression(@"\w+", ErrorMessage = "Специальные символы не разрешены")]
        [MaxLength(50, ErrorMessage = "Максимальная длина 50!")]
        public string Name { get; set; }

        [Display(Name = "День рождения")]
        [Required(ErrorMessage = "Введите вашу дату рождения")]
        [DataType(DataType.Date)]
        [DataValidatorAttribute(ErrorMessage = "Неправильная дата")]
        public DateTime Birthday { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Почта")]
        public string Email { get; set; }
    }
}
