﻿using Simple_Social_Media.BLL.DTO;
using System.Collections.Generic;

namespace Simple_Social_Media.Models
{
    public class FriendsViewModel
    {
        public string UserName { get; set; }
        public UserProfileViewModel UserProfileModel { get; set; }
        public IEnumerable<ContactDTO> Contacts { get; set; }
        public int ContactToDelete { get; set; }
    }
}