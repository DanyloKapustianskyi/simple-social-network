﻿using Simple_Social_Media.BLL.DTO;
using System.Collections.Generic;

namespace Simple_Social_Media.Models
{
    public class UserChatsViewModel
    {
        public string UserName { get; set; }
        public UserProfileViewModel UserProfile { get; set; }
        public IEnumerable<ChatDTO> Chats { get; set; }
    }
}
