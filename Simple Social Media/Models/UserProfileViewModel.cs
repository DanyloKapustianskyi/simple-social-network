﻿using Simple_Social_Media.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Simple_Social_Media.Models
{
    public class UserProfileViewModel
    {
        public string UserId { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Введите ваше имя")]
        [RegularExpression(@"[\w ]+", ErrorMessage = "Специальные символы не разрешены")]
        [MaxLength(50, ErrorMessage = "Максимальная длина 50!")]
        [MinLength(3, ErrorMessage = "Минимальная длина 3!")]
        public string UserName { get; set; }

        [Display(Name = "Почта")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Неправильный адресс электронной почты")]
        [MaxLength(50, ErrorMessage = "Максимальная длина 50!")]
        
        public string Email { get; set; }

        [Display(Name = "День рождения")]
        [DataType(DataType.Date)]
        [DataValidatorAttribute(ErrorMessage = "Неправильная дата")]
        [Required(ErrorMessage = "Выберите дату рождения")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Статус")]
        [RegularExpression(@"[a-zA-Z0-9-9А-Яа-яёЁїЇ\s\.\,\?\- ]+", ErrorMessage = "Специальные символы не разрешены")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100!")]
        public string Status { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Введите пожалуйста номер телефона")]
        [Phone(ErrorMessage = "Неправильный номер телефона")]
        [MinLength(10, ErrorMessage = "Минимальная длина 10!")]
        [MaxLength(15, ErrorMessage = "Максимальная длина 15!")]
        public string Phone { get; set; }

        public bool IsSuccess { get; set; }
    }
}