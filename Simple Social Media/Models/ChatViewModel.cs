﻿using Simple_Social_Media.BLL.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Simple_Social_Media.Models
{
    public class ChatViewModel
    {
        public int ChatId { get; set; }
        public string UserId { get; set; }
        public string FriendName { get; set; }
        public IEnumerable<MessageDTO> Messages { get; set; }

        [Required]
        [MaxLength(500, ErrorMessage = "Максимальная длина 500")]
        public string NewMessage { get; set; }
    }
}
