﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Simple_Social_Media.ValidationAttributes
{
    public class DataValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }
            else
            {
                DateTime dateTime = (DateTime)value;
                DateTime min = new DateTime(1900, 01, 01);
                if (dateTime < min || dateTime > DateTime.Now.AddYears(-10))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}