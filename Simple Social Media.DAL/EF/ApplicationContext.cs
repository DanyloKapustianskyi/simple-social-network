﻿using Microsoft.AspNet.Identity.EntityFramework;
using Simple_Social_Media.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        static ApplicationContext()
        {
            Database.SetInitializer(new ApplicationDbInitializer());
        }

        public ApplicationContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new ApplicationDbInitializer());
        }


        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Message> Messages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToOneConstraintIntroductionConvention>();

            modelBuilder.Entity<UserProfile>()
                .HasRequired(p => p.User)
                .WithOptional(c => c.UserProfile);

            modelBuilder.Entity<Contact>()
                .HasKey(p => p.ContactId);

            modelBuilder.Entity<Contact>()
                .Property(c => c.ContactId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Contact>()
                .HasRequired(c => c.User)
                .WithMany(c => c.Contacts);

            modelBuilder.Entity<Chat>()
                .HasRequired(c => c.Contact)
                .WithOptional(c => c.Chat);

            modelBuilder.Entity<Chat>()
                .Property(c => c.ChatId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Message>()
                .HasRequired(m => m.Chat)
                .WithMany(c => c.Messages);

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                IEnumerable<string> errors = ex.EntityValidationErrors.SelectMany(e => e.ValidationErrors).Select(e => e.ErrorMessage);
                string errorsMessage = string.Join(";", errors);
                string exception = string.Concat(ex.Message, "Validation errors: ", errorsMessage);
                throw new DbEntityValidationException(exception, ex.EntityValidationErrors);
            }
        }
    }
}
