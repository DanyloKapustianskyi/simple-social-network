﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Simple_Social_Media.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace Simple_Social_Media.DAL.EF
{
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            List<IdentityRole> identityRoles = new List<IdentityRole> {
                new IdentityRole () { Name = "Admin" },
                new IdentityRole() { Name = "User" }
            };
            foreach (IdentityRole role in identityRoles)
            {
                roleManager.Create(role);
            }
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var testUserProfile1 = new UserProfile { FullName = "test user", Birthday = DateTime.Now };
            var testApplicationUser1 = new ApplicationUser { Email = "test@gmail.com", UserName = "test@gmail.com", UserProfile = testUserProfile1, PhoneNumber = "0502223333" };
            userManager.Create(testApplicationUser1, "user123");
            userManager.AddToRole(testApplicationUser1.Id, "User");

            var testUserProfile2 = new UserProfile { FullName = "test user2", Birthday = DateTime.Now };
            var testApplicationUser2 = new ApplicationUser { Email = "test2@gmail.com", UserName = "test2@gmail.com", UserProfile = testUserProfile2, PhoneNumber = "0508887777" };
            userManager.Create(testApplicationUser2, "user123");
            userManager.AddToRole(testApplicationUser2.Id, "User");

            var testUserProfile3 = new UserProfile { FullName = "test user3", Birthday = DateTime.Now };
            var testApplicationUser3 = new ApplicationUser { Email = "test3@gmail.com", UserName = "test3@gmail.com", UserProfile = testUserProfile3, PhoneNumber = "0501111111" };
            userManager.Create(testApplicationUser3, "user123");
            userManager.AddToRole(testApplicationUser3.Id, "User");

            var testUserProfile4 = new UserProfile { FullName = "test user4", Birthday = DateTime.Now };
            var testApplicationUser4 = new ApplicationUser { Email = "test4@gmail.com", UserName = "test4@gmail.com", UserProfile = testUserProfile4, PhoneNumber = "0502222222" };
            userManager.Create(testApplicationUser4, "user123");
            userManager.AddToRole(testApplicationUser4.Id, "User");

            var adminUserProfile = new UserProfile { FullName = "test admin", Birthday = DateTime.Now };
            var adminApplicationUser = new ApplicationUser { Email = "admin@gmail.com", UserName = "admin@gmail.com", UserProfile = adminUserProfile, PhoneNumber = "0554444444" };
            userManager.Create(adminApplicationUser, "admin123");
            userManager.AddToRole(adminApplicationUser.Id, "Admin");

            var chat1 = new Chat();

            context.Chats.Add(chat1);

            var message1 = new Message { SenderId = testApplicationUser1.Id, Text = "Hello", Time = DateTime.Now.AddMinutes(-10), Chat = chat1 };
            var message2 = new Message { SenderId = testApplicationUser2.Id, Text = "Hello, bro", Time = DateTime.Now.AddMinutes(-8), Chat = chat1 };

            context.Messages.AddRange(new List<Message> { message1, message2 });

            //context.SaveChanges();

            Contact contact1 = new Contact { User = testApplicationUser1, Friend = testApplicationUser2, Chat = chat1 };
            Contact contact2 = new Contact { User = testApplicationUser2, Friend = testApplicationUser1, Chat = chat1 };

            context.Contacts.AddRange(new List<Contact> { contact1, contact2 });


            context.SaveChanges();
            base.Seed(context);
        }
    }
}
