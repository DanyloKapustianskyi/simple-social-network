﻿using Simple_Social_Media.DAL.Entities;

namespace Simple_Social_Media.DAL.Interfaces
{
    public interface IUserProfileRepository : IRepository<UserProfile,string>
    {
    }
}
