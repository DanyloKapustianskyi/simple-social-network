﻿using Simple_Social_Media.DAL.Entities;

namespace Simple_Social_Media.DAL.Interfaces
{
    public interface IContactRepository: IRepository<Contact,int>
    {
    }
}
