﻿using Simple_Social_Media.DAL.Entities;

namespace Simple_Social_Media.DAL.Interfaces
{
    public interface IChatRepository: IRepository<Chat,int>
    {
    }
}
