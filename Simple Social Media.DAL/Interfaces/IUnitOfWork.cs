﻿using Simple_Social_Media.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        ApplicationSignInManager SignInManager { get; }
        IChatRepository ChatRepository { get; }
        IMessageRepository MessageRepository { get; }
        IContactRepository ContactRepository { get; }
        IUserProfileRepository UserProfileRepository { get; }

        Task SaveAsync();
    }
}
