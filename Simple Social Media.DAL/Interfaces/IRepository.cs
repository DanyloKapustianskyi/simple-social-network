﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Interfaces
{
    public interface IRepository<T,Type> where T: class
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetById(Type id);
        void Create(T item);
        void Update(T item);
        void Delete(Type id);
        void Delete(T entity);
    }
}
