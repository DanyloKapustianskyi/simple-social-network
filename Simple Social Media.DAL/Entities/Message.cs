﻿using System;

namespace Simple_Social_Media.DAL.Entities
{
    public class Message
    {
        public int MessageId { get; set; }
        public Chat Chat { get; set; }
        public string Text { get; set; }
        public DateTime Time { get; set; }
        public string SenderId { get; set; }

    }
}
