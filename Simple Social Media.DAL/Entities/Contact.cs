﻿namespace Simple_Social_Media.DAL.Entities
{
    public class Contact
    {          
        public int ContactId { get; set; }
        public bool IsDeleted { get; set; }
        public ApplicationUser User { get; set; }
        public ApplicationUser Friend { get; set; }
        public Chat Chat { get; set; }
    }
}
