﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simple_Social_Media.DAL.Entities
{
    public class UserProfile
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string Status { get; set; }
        public ApplicationUser User { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime LastOnline { get; set; }
    }
}
