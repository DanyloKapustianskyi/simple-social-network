﻿using System.Collections.Generic;

namespace Simple_Social_Media.DAL.Entities
{
    public class Chat
    {
        public int ChatId { get; set; }

        public bool IsDeleted { get; set; }
        
        public ICollection<Message> Messages { get; set; }

        public Contact Contact { get; set; }
    }
}
