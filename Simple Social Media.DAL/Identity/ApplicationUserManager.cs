﻿using Microsoft.AspNet.Identity;
using Simple_Social_Media.DAL.Entities;

namespace Simple_Social_Media.DAL.Identity
{
    public class ApplicationUserManager: UserManager<ApplicationUser> 
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store): base(store) { }

    }
}
