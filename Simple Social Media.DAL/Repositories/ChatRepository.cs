﻿using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Repositories
{
    public class ChatRepository : Repository<Chat, int>, IChatRepository
    {
        public ChatRepository(ApplicationContext context) : base(context) { }

        public async override Task<IEnumerable<Chat>> GetAllAsync()
        {
            return await context.Chats
                .Include(c => c.Contact)
                .Include(c => c.Contact.User)
                .Include(c => c.Contact.Friend)
                .ToArrayAsync();
        }

        public async override Task<Chat> GetById(int id)
        {
            return await context.Chats
                .Where(c => c.ChatId == id)
                .Include(c => c.Contact)
                .Include(c => c.Contact.User)
                .Include(c => c.Contact.Friend)
                .FirstOrDefaultAsync();
        }
    }
}
