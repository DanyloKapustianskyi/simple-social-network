﻿using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System;

namespace Simple_Social_Media.DAL.Repositories
{
    public class ContactRepository : Repository<Contact, int>, IContactRepository
    {
        public ContactRepository(ApplicationContext context) : base(context) { }

        public override async Task<IEnumerable<Contact>> GetAllAsync()
        {
            return await context.Contacts
            .Include(c => c.User)
            .Include(c => c.Friend)
            .ToArrayAsync();
        }

        public override async Task<Contact> GetById(int id)
        {
            return await context.Contacts.Where(c => c.ContactId == id)
                .Include(c => c.User)
                .Include(c => c.Friend)
                .FirstOrDefaultAsync();
        }
    }
}
