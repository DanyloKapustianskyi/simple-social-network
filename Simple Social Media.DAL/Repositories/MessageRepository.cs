﻿using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Repositories
{
    public class MessageRepository : Repository<Message, int>, IMessageRepository
    {
        public MessageRepository(ApplicationContext context) : base(context) { }

        public async override Task<IEnumerable<Message>> GetAllAsync()
        {
            return await context.Messages
                .Include(m => m.Chat)
                .Include(m => m.Chat.Contact)
                .Include(m => m.Chat.Contact.Friend)
                .ToArrayAsync();
        }

        public async override Task<Message> GetById(int id)
        {
            return await context.Messages
                .Where(m => m.MessageId == id)
                .Include(m => m.Chat)
                .Include(m => m.Chat.Contact)
                .FirstOrDefaultAsync();
        }
    }
}
