﻿using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Repositories
{
    public class Repository<T, Type> : IRepository<T, Type> where T : class
    {
        protected ApplicationContext context;

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        public void Create(T item)
        {
            if (item != null)
            {
                context.Set<T>().Add(item);
            }
            else
            {
                throw new ArgumentNullException(nameof(item));
            }
        }

        public void Delete(Type id)
        {
            T item = context.Set<T>().Find(id);
            if (item == null)
            {
                throw new KeyNotFoundException(nameof(id));
            }
            context.Set<T>().Remove(item);
        }

        public void Delete(T entity)
        {
            if (entity != null)
            {
                context.Set<T>().Remove(entity);
            }
            else
            {
                throw new ArgumentNullException(nameof(entity));
            }
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await context.Set<T>().ToArrayAsync();
        }

        public virtual async Task<T> GetById(Type id)
        {
            return await context.Set<T>().FindAsync(id);
        }

        public void Update(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
            context.Set<T>().Attach(item);
            context.Entry(item).State = EntityState.Modified;
        }
    }
}
