﻿using Microsoft.AspNet.Identity.EntityFramework;
using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Identity;
using Simple_Social_Media.DAL.Interfaces;
using System;
using System.Threading.Tasks;
using System.Web;

namespace Simple_Social_Media.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;

        private readonly ApplicationContext context;

        private readonly ApplicationUserManager userManager;
        private readonly ApplicationRoleManager roleManager;
        private readonly ApplicationSignInManager signInManager;

        private readonly IChatRepository chatRepository;
        private readonly IMessageRepository messageRepository;
        private readonly IContactRepository contactRepository;
        private readonly IUserProfileRepository userProfileRepository;

        public UnitOfWork()
        {
            context = new ApplicationContext();
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));
            signInManager = new ApplicationSignInManager(userManager, HttpContext.Current.GetOwinContext().Authentication);
            chatRepository = new ChatRepository(context);
            messageRepository = new MessageRepository(context);
            contactRepository = new ContactRepository(context);
            userProfileRepository = new UserProfileRepository(context);
        }

        public ApplicationUserManager UserManager { get => userManager; }
        public ApplicationRoleManager RoleManager { get => roleManager; }
        public ApplicationSignInManager SignInManager { get => signInManager; }
        public IChatRepository ChatRepository { get => chatRepository; }
        public IMessageRepository MessageRepository { get => messageRepository; }
        public IContactRepository ContactRepository { get => contactRepository; }
        public IUserProfileRepository UserProfileRepository { get => userProfileRepository; }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
