﻿using Simple_Social_Media.DAL.EF;
using Simple_Social_Media.DAL.Entities;
using Simple_Social_Media.DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Simple_Social_Media.DAL.Repositories
{
    public class UserProfileRepository : Repository<UserProfile, string>, IUserProfileRepository
    {
        public UserProfileRepository(ApplicationContext context) : base(context) { }

        public override async Task<IEnumerable<UserProfile>> GetAllAsync()
        {
            return await context.UserProfiles.Include(u => u.User).ToArrayAsync();
        }

        public override async Task<UserProfile> GetById(string id)
        {
            return await context.UserProfiles.Where(u => u.Id.Equals(id)).Include(u => u.User).FirstOrDefaultAsync();
        }
    }
}
