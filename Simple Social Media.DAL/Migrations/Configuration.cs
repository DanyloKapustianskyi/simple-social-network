namespace Simple_Social_Media.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Simple_Social_Media.DAL.EF.ApplicationContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Simple_Social_Media.DAL.EF.ApplicationContext context)
        {
            Database.SetInitializer(new EF.ApplicationDbInitializer());
        }
    }
}
